package com.sample.boot2.service;

import com.sample.boot2.entity.PortalInfo;
import com.sample.boot2.repository.PortalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PortalInfoService {

    @Autowired
    private PortalInfoRepository repository;

    public PortalInfo findOne(int id) {
        return repository.findById(id).get();
    }

    public List<PortalInfo> findAll() {
        return repository.findAll();
    }
}
