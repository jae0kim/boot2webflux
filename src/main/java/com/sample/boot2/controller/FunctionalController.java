package com.sample.boot2.controller;

import com.sample.boot2.entity.PortalInfo;
import com.sample.boot2.repository.PortalInfoRepository;
import com.sample.boot2.service.PortalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Webflux.fn (Functional) 기반의 Router(Controller)
 * 기존 Controller 관련(url routing / mapping) 어노테이션이 없고
 * ServerResponse를 return하는 Method만 구현
 *
 * URL 을 Routing/Mapping 하는건 별도의 Configuration에서 담당
 * @see com.sample.boot2.config.RoutingConfig
 *
 * @author jykim
 */
@Component
public class FunctionalController {

    @Autowired
    private PortalInfoService service;

    private ServerResponse.BodyBuilder bodyBuilder = ServerResponse.ok().contentType(MediaType.APPLICATION_JSON);

    /**
     * 단일 결과 반환
     * @param request
     * @return
     */
    public Mono<ServerResponse> getOne(ServerRequest request) {
        int seq = Integer.parseInt(request.pathVariable("seq"));
        PortalInfo portalInfo = service.findOne(seq);
        Mono<PortalInfo> monoResult = Mono.just(portalInfo);

        return bodyBuilder.body(monoResult, PortalInfo.class);
    }

    /**
     * 복수 결과 반환
     * ServerResponse 는 Mono 결과 반환만 가능
     * 대신 body에 Flux로 발행된 결과를 담아서 반환
     *
     * ServerRequest 인자를 사용하지 않는다 하더라도 반드시 포함시켜야함
     * @see org.springframework.web.reactive.function.server.HandlerFunction
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> getAll(ServerRequest request) {
        List<PortalInfo> portalInfoList = service.findAll();
        Flux<PortalInfo> fluxResult = Flux.fromIterable(portalInfoList);

        return bodyBuilder.body(fluxResult, PortalInfo.class);
    }
}
