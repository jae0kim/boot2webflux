package com.sample.boot2.controller;

import com.sample.boot2.entity.PortalInfo;
import com.sample.boot2.service.PortalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * Annotation 기반의 Router(Controller)
 * GetMapping 은 RequestMapping(method = RequestMethod.GET) 의 shortcut(since 4.3)
 *
 * @author jykim
 */
@RestController
@RequestMapping("/anno")
public class AnnotationalController {

    @Autowired
    private PortalInfoService service;

    /**
     * 단일 결과 반환
     * @param seq
     * @return
     */
    @GetMapping("/findOne/{seq}")
    public Mono<PortalInfo> getOne(@PathVariable Integer seq) {
        PortalInfo portalInfo = service.findOne(seq);

        // 단일 객체를 Mono로 단순발행(just)
        return Mono.justOrEmpty(portalInfo);
    }

    /**
     * Mono 로 복수 결과(iter) 반환
     * @return
     */
    @GetMapping("/findAllMono")
    public Mono<List<PortalInfo>> getAllMono() {
        List<PortalInfo> portalInfoList = service.findAll();
        return Mono.justOrEmpty(portalInfoList);
    }

    /**
     * Flux 로 복수 결과(iter) 반환
     * @return
     */
    @GetMapping("/findAll")
    public Flux<PortalInfo> getAll() {
        List<PortalInfo> portalInfoList = service.findAll();

        // list를 Flux로 발행
        return Flux.fromIterable(portalInfoList);
    }
}
