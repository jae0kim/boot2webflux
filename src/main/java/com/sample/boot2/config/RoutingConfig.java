package com.sample.boot2.config;

import com.sample.boot2.controller.FunctionalController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

/**
 * Controller를 Webflux.fn 방식으로 구현하기 위한 설정
 *
 * @author jykim
 */
@Configuration
public class RoutingConfig {

    /**
     * URL Routing / Method Mapping
     * @param functionalController
     * @return
     */
    @Bean
    public RouterFunction<ServerResponse> routerFunction(FunctionalController functionalController) {
        return route(GET("/fn/findOne/{seq}").and(accept(MediaType.APPLICATION_JSON)), functionalController::getOne)
                .andRoute(GET("/fn/findAll").and(accept(MediaType.APPLICATION_JSON)), functionalController::getAll);
    }
}
