package com.sample.boot2.repository;

import com.sample.boot2.entity.PortalInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Sample Repository
 *
 * @author jykim
 */
@Repository
public interface PortalInfoRepository extends JpaRepository<PortalInfo, Integer> {
}
