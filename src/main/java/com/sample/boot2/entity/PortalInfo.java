package com.sample.boot2.entity;

import javax.persistence.*;

/**
 * Sample Entity
 *
 * @author jykim
 */
@Entity
@Table(name = "portal_info")
public class PortalInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int seq;
    private String name;
    private String baseUrl;

    public PortalInfo() {
    }

    public PortalInfo(int seq, String name, String baseUrl) {
        this.seq = seq;
        this.name = name;
        this.baseUrl = baseUrl;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
