package com.sample.boot2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.boot2.config.RoutingConfig;
import com.sample.boot2.controller.AnnotationalController;
import com.sample.boot2.controller.FunctionalController;
import com.sample.boot2.entity.PortalInfo;
import com.sample.boot2.service.PortalInfoService;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.BDDMockito.given;

/**
 * 호출 Test
 * Webflux.fn 방식의 컨트롤러를 테스트 하기 위해서는
 * URI 라우팅 설정 파일과 컨트롤러를 모두 import 해줘야 함
 * (default @WebFluxTest 만 사용시 status 404 return)
 */
@RunWith(SpringRunner.class)
@WebFluxTest({AnnotationalController.class, RoutingConfig.class, FunctionalController.class})
public class ControllerTest {

    @Autowired
    private WebTestClient client;
    @MockBean
    private PortalInfoService service;

    private Logger logger = LoggerFactory.getLogger(ControllerTest.class);

    private static Map<Integer, PortalInfo> data = new HashMap<>();
    private static final String URI_ANNOTATIION = "/anno";
    private static final String URI_FUNCTIONAL= "/fn";
    private static final String URI_FIND_ONE = "/findOne/{seq}";
    private static final String URI_FIND_ALL = "/findAll";


    @BeforeClass
    public static void setting() {
        data.put(1, new PortalInfo(1, "google", "https://www.google.com"));
        data.put(2, new PortalInfo(2, "naver", "https://www.naver.com"));
        data.put(3, new PortalInfo(3, "daum", "https://www.daum.com"));
    }

    /**
     * 어노테이션/findOne 호출 테스트
     * @throws JsonProcessingException
     */
    @Test
    public void annoFindOneTest() throws JsonProcessingException {
        final int testSeq = 1;
        this.findOne(URI_ANNOTATIION, testSeq);
    }

    /**
     * 어노테이션/findAll 호출 테스트
     * @throws JsonProcessingException
     */
    @Test
    public void annoFindAllTest() throws JsonProcessingException{
        this.findAll(URI_ANNOTATIION);
    }

    /**
     * 함수형/findOne 호출 테스트
     * @throws JsonProcessingException
     */
    @Test
    public void fnFindOneTest() throws JsonProcessingException {
        final int testSeq = 2;
        this.findOne(URI_FUNCTIONAL, testSeq);
    }

    /**
     * 함수형/findAll 호출 테스트
     * @throws JsonProcessingException
     */
    @Test
    public void fnFindAllTest() throws JsonProcessingException {
        this.findAll(URI_FUNCTIONAL);
    }

    private void findOne(String prefixURI, int testSeq) throws JsonProcessingException {
        given(service.findOne(testSeq)).willReturn(data.get(testSeq));

        String expectedResult = this.objectToJsonResult(data.get(testSeq));
        String uri = prefixURI+URI_FIND_ONE;

        logger.info(uri);

        client.get().uri(uri, testSeq).accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(expectedResult);
    }

    private void findAll(String prefixURI) throws JsonProcessingException{
        List<PortalInfo> dataList = Arrays.asList(data.get(1), data.get(2), data.get(3));
        given(service.findAll()).willReturn(dataList);

        String expectResult = this.objectToJsonResult(dataList);
        String uri = prefixURI+URI_FIND_ALL;

        logger.info(uri);

        client.get().uri(uri).accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(String.class)
                .isEqualTo(expectResult);
    }

    /**
     * object to json converter (using Jackson Lib)
     * @param obj
     * @return
     * @throws JsonProcessingException
     */
    private String objectToJsonResult(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }
}
